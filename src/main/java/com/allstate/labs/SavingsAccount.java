package com.allstate.labs;

public class SavingsAccount extends Account{
    @Override
    public void addInterest()
    {
       
        double balance = super.getBalance() * 1.4;
        super.setBalance(balance);
    }

    public SavingsAccount(String name, double balance) {
        super(name, balance);
    }
}
