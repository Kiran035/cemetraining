package com.allstate.labs;

public class HomeInsurance implements Detailable {
    double amountInsured;
    double excess;
    double premium;

    @Override
    public void getDetails() {
       System.err.printf("Insurance Premium is %s and Coverage is %", this.amountInsured, this.premium);
    }

    public HomeInsurance(double amountInsured, double excess, double premium) {
        this.amountInsured = amountInsured;
        this.excess = excess;
        this.premium = premium;
    }

}
