package com.allstate.labs;

public class CurrentAccount extends Account {
    
    @Override
    public void addInterest()
    {
       
        double balance = super.getBalance() * 1.1;
        super.setBalance(balance);
    }

    public CurrentAccount(String name, double balance) {
        super(name, balance);
    }

}
