package com.allstate.labs.Spring;

public interface Engine {
    double getEngineSize();
    void setEngineSize(double engineSize);
}
   
