package com.allstate.labs.Spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.allstate.labs.Spring")
public class CarConfig {
   /*  @Bean
    public Car car(@Autowired Engine engine)
    {
        return new Car();
    }
    @Bean
    public Engine engine()
    {
        return new PetrolEngine();
    } */
}
