package com.allstate.labs;

public abstract class Account implements Detailable{
    
    private double balance;
    private String name;
    private static double interestRate=1.1;

    public Account(String name, double balance) {
        this.balance = balance;
        this.name = name;
    }

    public Account() {
       this("",50);
    }
    
    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    abstract public void addInterest();

    public void display()
    {
        System.out.printf("My name is %s and balance is %.2f" ,this.name,this.balance);
        System.out.println();
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    @Override
    public void getDetails() {
       System.err.printf("Account holder name is %s and Balance is %", this.name, this.balance);
    }

   
    public boolean withdraw(int amount)
    {
        if(this.balance>=amount)
        {
            this.balance-=amount;
            System.out.println("Trasaction Success! balance = " +this.balance);
            return true;
        }
        else
        {
            System.out.println("Trasaction Not Success -In suficient balance, balance = " + this.balance);
            return false;
        }
    }

    public boolean withdraw()
    {
        return this.withdraw(20);
    }

}
