package com.allstate.dependencyInjuction.UsingExplicitConfiFile;

public class Dog implements Pet{

    @Override
    public void feed() {
       System.out.println("Feed the dog");
    }
    
}
