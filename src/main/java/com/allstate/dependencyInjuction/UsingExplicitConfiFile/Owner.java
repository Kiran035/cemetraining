package com.allstate.dependencyInjuction.UsingExplicitConfiFile;

public class Owner {
    private int id;
    private String name;
    private Pet pet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Owner(Pet pet) {
        this.pet = pet;
    }

    public Owner() {
       
    }

   /*  public Owner(int id, String name, Pet pet) {
        this.id = id;
        this.name = name;
        this.pet = pet;
    } */
}
