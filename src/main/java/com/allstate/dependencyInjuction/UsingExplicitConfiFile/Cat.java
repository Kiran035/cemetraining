package com.allstate.dependencyInjuction.UsingExplicitConfiFile;

public class Cat implements Pet{

    @Override
    public void feed() {
       System.out.println("Feed the cat");
    }
}
