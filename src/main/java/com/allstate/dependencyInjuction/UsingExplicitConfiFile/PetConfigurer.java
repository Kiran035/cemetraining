package com.allstate.dependencyInjuction.UsingExplicitConfiFile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class PetConfigurer {
    @Bean
    public Owner owner(@Autowired Pet pet)
    {
        return new Owner(pet);
    }
    
    @Bean
    public Pet pet(){
        return new Cat();
    }

    
}
