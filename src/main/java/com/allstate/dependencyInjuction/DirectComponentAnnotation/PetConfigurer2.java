package com.allstate.dependencyInjuction.DirectComponentAnnotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("com.allstate.dependencyInjuction.DirectComponentAnnotation")
public class PetConfigurer2 {
  
}
