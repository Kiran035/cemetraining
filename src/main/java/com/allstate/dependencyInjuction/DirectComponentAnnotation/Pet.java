package com.allstate.dependencyInjuction.DirectComponentAnnotation;

public interface Pet {
    void feed();
}
