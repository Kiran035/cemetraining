package com.allstate.dependencyInjuction.DirectComponentAnnotation;

import org.springframework.stereotype.Component;

@Component("Cat")
public class Cat implements Pet{

    @Override
    public void feed() {
       System.out.println("Feed the cat");
    }
}
