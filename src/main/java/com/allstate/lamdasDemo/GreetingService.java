package com.allstate.lamdasDemo;

public interface GreetingService {
    void sayMessage(String msg);
}
