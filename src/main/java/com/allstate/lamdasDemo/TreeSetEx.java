package com.allstate.lamdasDemo;

import java.util.*;

import com.allstate.entities.Employee;

public class TreeSetEx {
    public static void main(String[] args) {
        Employee e1 = new Employee(1, "ccc", 32, "", 12000);
        Employee e2 = new Employee(1, "aaa", 32, "", 12000);
        Employee e3 = new Employee(1, "bb", 32, "", 12000);
        Comparator<Employee> comp = (o1,o2)-> o1.getName().compareTo(o2.getName());
       
        Set<Employee> list = new TreeSet<>(comp);
        list.add(e1);
        list.add(e2);
        list.add(e3);
       for(Employee e : list) 
          System.out.println(e.getName());
    }
}
