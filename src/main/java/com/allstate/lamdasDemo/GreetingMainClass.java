package com.allstate.lamdasDemo;

import java.util.*;
import java.util.stream.Collectors;

import com.allstate.labs.Detailable;
public class GreetingMainClass {
    public static void main(String[] args) {
        
        GreetingService gs = (str) -> System.out.println("Hello " + str);
        gs.sayMessage("Kiran");

        Detailable d = () -> System.out.println("Hello");
        d.getDetails();

        List<String> names = new ArrayList<String>();
        names.add("Welcome");
        names.add("Hello");
        names.add("Hi");
        names.add("Bye");
       
        names.forEach(GreetingMainClass::display);
        names.forEach(str->display(str));
        names.forEach(str->System.out.println(str));

        Comparator<String> comp = (o1,o2)-> Integer.compare(o1.length(), o2.length());
        names.sort(comp);
        System.out.println("Sorting =" + names);

         List<String> filter = names.stream().filter(str -> str.length() > 2).collect(Collectors.toList());
         System.out.println("Filter Result = " + filter);

         List<String> map = names.stream().map(s->s.toUpperCase()).collect(Collectors.toList());
         System.out.println(" MAp result = " + map);

    }

    public static void display(String str)
    {
        System.out.println(str);
    }
}
