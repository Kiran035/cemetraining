package com.allstate.dao;

import com.allstate.entities.Employee;
import java.util.List;
public interface Db {
    
    long count();
    Employee find(int id);
    List<Employee> findall();
    void save(Employee employee);
}
