package com.allstate.dao;

import com.allstate.dependencyInjuction.DirectComponentAnnotation.Owner2;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OwnerRepository extends MongoRepository<Owner2,ObjectId> {
    
}
